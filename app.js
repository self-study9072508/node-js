const express = require("express");

const app = express();
const port = 3000;
const userRoutes = require("./api/routes/user");

// ================================
var options = {
  dotfiles: "ignore",
  etag: false,
  extensions: ["htm", "html"],
  index: false,
  maxAge: "1d",
  redirect: false,
  setHeaders: function (res, path, stat) {
    res.set("x-timestamp", Date.now());
    res.set("Content-Type", "application/json");
  },
};
app.use(express.static("public", options));

// ================================
app.use("/user", userRoutes);

// ================================
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

// ================================

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
