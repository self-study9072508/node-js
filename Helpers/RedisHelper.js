const config = require("config");
module.exports = class ResHelper {

  /**
   * It creates a connection to the redis server.
   */
  constructor() {
    // ======================================================
    this.redis = require("redis");
    this.client = this.redis.createClient(config.redis);
    // ======================================================
  }
};
