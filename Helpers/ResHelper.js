const config = require("config");
module.exports = class ResHelper {
  /**
   * The constructor function is called when a new instance of the class is created
   */
  constructor() {}

  /**
   * 統一輸出結構
   * It returns a response with a status code and a json object.
   * @param res - The response object
   * @param http_code - The HTTP status code to return.
   * @param _json - The JSON object to be sent to the client.
   * @returns the response object with the status code and the json object.
   */
  out_res(res, http_code, _json) {
    return res.status(http_code).send(_json);
  }

  /**
   * 建立回傳格式
   * It creates a response object.
   * @param [code=404] - The status code of the response.
   * @param [_data] - The data returned by the interface, which is the data returned by the interface.
   * @returns An object with a code, message, and data property.
   */
  create_res(code = 200, _data = null || undefined) {
    return {
      code: code != 200 ? code : 200,
      message: code != 200 ? config.BodyCode[code] : config.BodyCode[code],
      data: _data ?? {},
    };
  }
};
