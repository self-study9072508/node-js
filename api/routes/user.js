const express = require("express");
const router = express.Router();

const UserController = require("../controllers/user");
const checkAuth = require("../middleware/check-auth");

router.get("/userInfo", UserController.user_info);
router.get("/gd", UserController.gd);

module.exports = router;
