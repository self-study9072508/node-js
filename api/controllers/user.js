var svgCaptcha = require("svg-captcha");
//
let ResHelperClass = require("../../Helpers/ResHelper");
const ResHelper = new ResHelperClass();
//
const redis_class = require("../../Helpers/RedisHelper");
let redis_client = new redis_class();

//
const userList = require("../../mock/user");

//
exports.user_info = async (request, response, next) => {
  // console.log(config);
  let status = 404;
  let user_info = {};

  // console.log(request.query.id);
  // 檢查參數是否存在
  if (request.query.id === undefined) {
    status = 503;
  } else {
    // mock data
    userList.forEach((item) => {
      if (item.id == request.query.id) {
        // console.log(item.id);
        user_info = item;
      }
    });

    //判断是否有结果
    if (Object.keys(user_info).length != 0) {
      status = 200;
    }
  }

  return ResHelper.out_res(
    response,
    status,
    ResHelper.create_res(
      status,
      status == 503
        ? {}
        : {
            params: request.params,
            query: request.query,
            user_info: user_info,
          }
    )
  );
};

exports.gd = async (request, response, next) => {
  const max = 3;
  const min = 1;
  var captcha = svgCaptcha.create({
    size: 4, //資料長度
    ignoreChars: "0oO1iIlL", //排除資料
    noise: Math.floor(Math.random() * (max - min) + min), //噪點 就是圖片線條
    color: true,
    background: "#cc9966",
  });
  // console.log(captcha.text);
  // console.log(redis_client);
  await redis_client.client.connect();
  await redis_client.client.set("captcha", captcha.text);
  const value = await redis_client.client.get("captcha");
  response.type("svg");
  response.status(200).send(captcha.data);
  await redis_client.client.disconnect();
};
